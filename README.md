Dissertation Project
==========================

"Isometric cross-platform story-driven game"

This project shows a Script-based engine for making interactive story-driven gameplay based on Cocos2DX. 

The majority of script logic can be found in 'ATL objects' which are built by the ScriptManager class. ATL objects have links to actual game world objects to manipulate the environment and change maps

A report of this project can be found [here](https://drive.google.com/open?id=1BCUM1Q-RcZQ-R3UPnGIjKXiCfUwSfZY1_NWkmj8kGd4)

Appendices [here](https://drive.google.com/open?id=1fa_f5ovq9Hm4THjmuBrhgfm-XtP-IoXKYMwKeydYgi4)


Screenshots
==========================
![Screen Shot 2015-08-24 at 5.02.19 PM.png](https://bitbucket.org/repo/pM6rX9/images/826363023-Screen%20Shot%202015-08-24%20at%205.02.19%20PM.png)
![Screen Shot 2015-08-24 at 5.02.26 PM.png](https://bitbucket.org/repo/pM6rX9/images/672524572-Screen%20Shot%202015-08-24%20at%205.02.26%20PM.png)


Changes required to cocos2dx in order to run
================

OSX
---
CCFileUtilsApple.mm

modify:
`FileUtilsApple::getFullPathForDirectoryAndFilename`

to:
```
std::string FileUtilsApple::getFullPathForDirectoryAndFilename(const std::string& strDirectory, const std::string& strFilename)
{
    if (strDirectory[0] != '/')
    {
        NSString* fullpath = [[NSBundle mainBundle] pathForResource:[NSString stringWithUTF8String:strFilename.c_str()]
                                                             ofType:nil
                                                        inDirectory:[@"Resources/" stringByAppendingString:[NSString stringWithUTF8String:strDirectory.c_str()]]];
        if (fullpath != nil) {
            return [fullpath UTF8String];
        }
    }
    else
    {
        std::string fullPath = strDirectory+strFilename;
        // Search path is an absolute path.
        if ([s_fileManager fileExistsAtPath:[NSString stringWithUTF8String:fullPath.c_str()]]) {
            return fullPath;
        }
    }
    return "";
}
```