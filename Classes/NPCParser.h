#ifndef __DS_NPCPARSER__
#define __DS_NPCPARSER__

#include "cocos2d.h"
#include "NPC.h"
#include "FileReader.h"
#include "ATSNode.h"
#include "DSScript.h"

USING_NS_CC;

static NPC* parseNPCfromFile(String npcFileName){
std:stringstream ss;
	ss << "NPCs/NPC/" << npcFileName.getCString();
	vector<string> lines = FileReader::ReadFile(ss.str().c_str(), 'r');

	String* name = nullptr;
	String* displayName = nullptr;
	String* expression = new String("default");
	String* spawn = new String("default");
	
	vector<string> initScriptLines = vector<string>();
	vector<string> mainScriptLines = vector<string>();
	vector<string> interactScriptLines = vector<string>();

	bool readingInit = false;
	bool readingMain = false;
	bool readingInteract = false;

	for (string line : lines){
		if (line.length() >= 2 && strcmp(line.substr(0, 2).c_str(), "\r") != 0){
			if (strcmp(line.substr(0, 1).c_str(), "#") != 0)
			{
				if (readingInit || readingInteract || readingMain)
				{
					//End of script
					if (line.length() > 1 && strcmp(line.substr(0, 2).c_str(), ":!") == 0)
					{
						readingInit = false;
						readingMain = false;
						readingInteract = false;
					}
					else
					{
						line.erase(line.find_last_not_of(" \n\r\t") + 1);
						if (readingInit)
							initScriptLines.push_back(line);
						else if (readingMain)
							mainScriptLines.push_back(line);
						else if (readingInteract)
							interactScriptLines.push_back(line);
					}
				}
				else
				{

					if (line.length() > 6 && strcmp(line.substr(0, 5).c_str(), "Name:") == 0){
						string param = line.substr(6);
						param.erase(param.find_last_not_of(" \n\r\t") + 1);
						name = new String(param);
					}

					if (line.length() > 7 && strcmp(line.substr(0, 6).c_str(), "Spawn:") == 0){
						string param = line.substr(7);
						param.erase(param.find_last_not_of(" \n\r\t") + 1);
						spawn = new String(param);
					}

					if (line.length() > 13 && strcmp(line.substr(0, 12).c_str(), "DisplayName:") == 0){
						string param = line.substr(13);
						param.erase(param.find_last_not_of(" \n\r\t") + 1);
						displayName = new String(param);
					}

					if (line.length() > 12 && strcmp(line.substr(0, 11).c_str(), "Expression:") == 0){
						string param = line.substr(12);
						param.erase(param.find_last_not_of(" \n\r\t") + 1);
						expression = new String(param);
					}

					//load scripts
					if (line.length() > 4 && strcmp(line.substr(0, 5).c_str(), "Init:") == 0){
						readingInit = true;
					}
					if (line.length() > 4 && strcmp(line.substr(0, 5).c_str(), "Main:") == 0){
						readingMain = true;
					}
					if (line.length() > 8 && strcmp(line.substr(0, 9).c_str(), "Interact:") == 0){
						readingInteract = true;
					}
				}
			}
		}
		else
		{
			//Empty line

		}
	}

	DSScript* initScript = nullptr;
	DSScript* mainScript = nullptr;
	DSScript* interactScript = nullptr;

	if (initScriptLines.size() > 0)
		initScript = new DSScript(initScriptLines, string("npc"), string(npcFileName.getCString()));
	if (mainScriptLines.size() > 0)
		mainScript = new DSScript(mainScriptLines, string("npc"), string(npcFileName.getCString()));
	if (interactScriptLines.size() > 0)
		interactScript = new DSScript(interactScriptLines, string("npc"), string(npcFileName.getCString()));

	return new NPC(initScript, interactScript, mainScript, name, new String(npcFileName), displayName, expression, spawn);
}

static vector<NPC*>* parseNPCsFromNPCString(String* npcstr){
	vector<NPC*>* npcs = new vector<NPC*>();

	string line = string(npcstr->getCString());
	std::istringstream ss(line);
	std::istream_iterator<std::string> begin(ss), end;
	std::vector<std::string> arrayTokens(begin, end);
	for (string s : arrayTokens)
	{
		NPC* npc = parseNPCfromFile(String(s.c_str()));
		npcs->push_back(npc);
	}
	return npcs;
}

#endif 
