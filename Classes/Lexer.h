#ifndef __DS_LEXER__
#define __DS_LEXER__

#include "cocos2d.h"
#include "DSScript.h"
#include "Parser.h"

#include "AASay.h"
USING_NS_CC;

class DSScript;

class Lexer
{
public:
	Lexer(){};
	~Lexer(){};
	ATSNode* getNextNode(DSScript* script, Parser* parser);

private:

};

#endif