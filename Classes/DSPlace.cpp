#include "DSPlace.h"

USING_NS_CC;

bool DSPlace::init()
{

	return true;
}

bool DSPlace::loadTMXMap(){
	if (m_mapPath == NULL){
		CCLOG("Trying to load TMXmap before DSPlace is initialized");
		return false;
	}
	m_tmxMap = TMXTiledMap::create(m_mapPath->getCString());
	return m_tmxMap != NULL;
}

void DSPlace::process(){
	//process spawns
	CCTMXObjectGroup* objectGroup = m_tmxMap->getObjectGroup("_spawns");
	if (objectGroup == NULL){
		CCLOG("%s", "FATAL! No Spawn object layer found");
		return;
	}
	CCTMXLayer* spawns = m_tmxMap->getLayer("_player");
	spawnZ= spawns->getZOrder();
	for (int i = 0; i < objectGroup->getObjects()->count();i++)
	{
		Dictionary* d = static_cast<CCDictionary*>(objectGroup->getObjects()->objectAtIndex(i));
		String* name = static_cast<String*>(d->objectForKey("name"));
		m_spawns->setObject((Object*) d, name->getCString());
	}

}

DSPlace::~DSPlace(){
	m_tag->release();
	m_name->release();
	m_mapPath->release();
	m_lighting->release();
	m_npcs->release();
	CC_SAFE_RELEASE(m_tmxMap);
}