#include "AAMapEvent.h"

void AAMapEvent::runATS(TileLayer* tileLayer, UILayer* uiLayer, VNLayer* vnLayer){
	if (!_isDone)
	{
		if (_isDelete)
		{
			tileLayer->deleteTileAtObjectNamed(targetObject);
		}
		_isDone = true;
	}
}

bool AAMapEvent::isDone(){
	return _isDone;
}