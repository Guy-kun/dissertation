#ifndef __DS_PLAYER__
#define __DS_PLAYER__

#include "cocos2d.h"
#include "NPC.h"
#include "ATSNode.h"

USING_NS_CC;

class Player : public NPC
{
public:
	Player(String* name, String* expression,String* displayName,String* spawnPoint) :NPC(nullptr, nullptr, nullptr, name, new String("player"),displayName,expression,spawnPoint){};
	~Player(){};


private:

};

#endif 
