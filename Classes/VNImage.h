#ifndef __VN_IMAGE__
#define __VN_IMAGE__

#include "cocos2d.h"

USING_NS_CC;

//Subclass of CCSprite, for supporting additional functionality of onscreen images

class VNImage : public CCSprite
{
public:
	static VNImage* create(String name, String expression);
	~VNImage(){};
	VNImage(String name, String expression);

	String getName(){ return _name; };
	String getExpression(){ return _expression; };
private:
	String _name;
	String _expression;
};

#endif 
