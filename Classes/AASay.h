#ifndef __DS_AASAY__
#define __DS_AASAY__

#include "cocos2d.h"
#include "ATSNode.h"
#include "TileLayer.h"
#include "UILayer.h"
USING_NS_CC;

class DSScript;

class AASay : public ATSNode
{
public:
	AASay(DSScript* script): ATSNode(script){
		_isExternallyDone = false;
	};
	~AASay(){};

	String getWho(){ return who; };
	String getWhat(){ return what; };
	void setWho(String w){ who = w; };
	void setWhat(String w){ what = w; };

	void runATS  (TileLayer* tileLayer,UILayer* uiLayer, VNLayer* vnLayer) override;
	bool isDone();
private:
	String who;
	String what;
};

#endif