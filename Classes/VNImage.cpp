#include "VNImage.h"

using namespace std;

VNImage* VNImage::create(String name, String expression)
{
	VNImage *img = new VNImage(name,expression);

	//Build path from name and expression
	std:stringstream ss;
	ss << "VNImages/" << name.getCString() << "/" << expression.getCString() << ".png";

	img->initWithFile(ss.str().c_str());
	img->setAnchorPoint(ccp(0.5, 0));
	img->autorelease();

	return img;
}

VNImage::VNImage(String name, String expression)
{
	_name = name;
	_expression = expression;
}