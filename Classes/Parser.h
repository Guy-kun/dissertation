#ifndef __DS_PARSER__
#define __DS_PARSER__

#include "cocos2d.h"
#include "ATSNode.h"
#include "DSScript.h"
#include "AAUICommand.h"
#include "AASay.h"
#include "AAShowHide.h"
#include "AAMapEvent.h"
#include "AAChangeMap.h"
#include "AALightingCommand.h"
#include "AANPCCommand.h"
#include "AAAudioCommand.h"
USING_NS_CC;

class Parser
{
public:
	Parser(){};
	~Parser(){};

	ATSNode* getNextNode(DSScript* script);

private:
	
};

#endif