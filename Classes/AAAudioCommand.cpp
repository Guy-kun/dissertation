#include "AAAudioCommand.h"

void AAAudioCommand::runATS(TileLayer* tileLayer, UILayer* uiLayer, VNLayer* vnLayer){
	if (!_isDone)
	{
		if (isBGM)
		{
			if (isStop)
				tileLayer->stopBGM();
			else
				tileLayer->playBGM(audioName, volume);
		}
		else
		{
			tileLayer->playSFX(audioName, volume);
		}
		_isDone = true;
	}
}

bool AAAudioCommand::isDone(){
	return _isDone;
}