#ifndef __DS_NPC__
#define __DS_NPC__

#include "cocos2d.h"
#include "ATSNode.h"
#include "DSScript.h"

USING_NS_CC;

enum Direction{
	NE, NW, SE, SW
};

/*
	NPCs inherit from Node so that they can be added to TileLayer and benefit from layer repositioning with camera. Their sprites are position related to node position when drawn
*/
class NPC : public cocos2d::Node
{
public:
	NPC(DSScript* init, DSScript* interact, DSScript* main, String* npcName, String* npcTag, String* displayname, String* expr, String* spawnpoint);
	NPC(){};
	~NPC();

	//Passed map size to check bounds
	void moveNE(Size mapSize,Size tileSize);
	void moveNW(Size mapSize, Size tileSize);
	void moveSE(Size mapSize, Size tileSize);
	void moveSW(Size mapSize, Size tileSize);

	void turn(Direction dir){ currentDirection = dir; }
	void visit();

	Sprite* getCurrentSprite(){
		switch (currentDirection){
		case NE:
			return neSprite;
		case NW:
			return nwSprite;
		case SE:
			return seSprite;
		case SW:
			return swSprite;
		}
	};

	Direction getDirection(){ return currentDirection; };

protected:
	//Run when NPC is added to map
	CC_SYNTHESIZE_RETAIN(DSScript*, initScript, InitScript);
	//Run when NPC is interacted with
	CC_SYNTHESIZE_RETAIN(DSScript*, interactScript, InteractScript);
	//Run as a loop when NPC is on map
	CC_SYNTHESIZE_RETAIN(DSScript*, mainScript, MainScript);

	//Hold references to current expression's directional sprites to avoid holding all expressions in cache
	//cc2dx will remove unused/unreferenced sprites when necessary
	CC_SYNTHESIZE_RETAIN(Sprite*, neSprite, NESprite);
	CC_SYNTHESIZE_RETAIN(Sprite*, nwSprite, NWSprite);
	CC_SYNTHESIZE_RETAIN(Sprite*, swSprite, SWSprite);
	CC_SYNTHESIZE_RETAIN(Sprite*, seSprite, SESprite);

	CC_SYNTHESIZE_RETAIN(String*, npctag, NPCTag);
	CC_SYNTHESIZE_RETAIN(String*, name, Name);
	CC_SYNTHESIZE_RETAIN(String*, displayName, DisplayName);
	CC_SYNTHESIZE_RETAIN(String*, expression, Expression);
	CC_SYNTHESIZE_RETAIN(String*, spawn, Spawn);


	Direction currentDirection;

private:
	void setNewDirection(Direction dir);
	void loadExpression(String* expression);

	Sprite* loadSprite(String filename);

	bool checkMovementBounds(Size mapSize, Size tileSize, Point newPos);
};

#endif 
