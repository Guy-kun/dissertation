#include "ATSNode.h"
#include "TileLayer.h"

void ATSNode::runATS(TileLayer* tileLayer, UILayer* uiLayer, VNLayer* vnLayer){
	for (int i = 0; i < children->count(); i++){
		static_cast<ATSNode*>(children->objectAtIndex(i))->runATS(tileLayer, uiLayer,vnLayer);
	}
};