#ifndef __MAINMENU_SCENE_H__
#define __MAINMENU_SCENE_H__

#include "cocos2d.h"

USING_NS_CC;

class MainMenuScene : public cocos2d::Scene
{
public:

	virtual bool init();

	static cocos2d::Scene* scene();
	void menuCloseCallback(Object* pSender);

	CREATE_FUNC(MainMenuScene);
	~MainMenuScene();

	void onTouchesEnded(std::vector<CCTouch*> pTouches, cocos2d::CCEvent *pEvent);
	void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	void keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

private:
	CCSprite* startScreen;
};

#endif 
