#include "DialogueBox.h"

USING_NS_CC;

void DialogueBox::cleanupText(){
	if (currentText != nullptr&&currentName != nullptr)
	{
		removeChild(currentText, true);
		removeChild(currentName, true);
	}
}

Layer* DialogueBox::layer()
{
	Layer *layer = Layer::create();
	return layer;
}

bool DialogueBox::init()
{
	currentText = nullptr;
	currentName = nullptr;
		
	isInDialogueMode = false;
	boxImage = CCSprite::create();
	boxImage->initWithFile("Images/dialoguebox.png");
	boxImage->retain();
	boxImage->setAnchorPoint(ccp(0, 0));
	boxImage->setPosition(ccp(0, 0));

	setAnchorPoint(ccp(0, 0));
	setPosition(ccp(0, 0));

	//enterDialogueMode();
	return true;
}

void DialogueBox::enterDialogueMode(){
	if (!isInDialogueMode)
	{
		isInDialogueMode = true;
		addChild(boxImage);
	}
}

void DialogueBox::leaveDialogueMode(){
	if (isInDialogueMode)
	{
		cleanupText();
		isInDialogueMode = false;
		removeChild(boxImage);
	}
}
void DialogueBox::say(String character, String text){
	cleanupText();

	currentName = CCLabelTTF::create(character.getCString(), "Helvetica", 22,
		Size(500, 40), kCCTextAlignmentLeft,
		kCCVerticalTextAlignmentBottom);
	currentName->setAnchorPoint(ccp(0, 0));
	currentName->setPosition(ccp(250, 140));

	currentText = CCLabelTTF::create(text.getCString(), "Helvetica", 19,
		Size(550, 190), kCCTextAlignmentLeft,
		kCCVerticalTextAlignmentTop);
	currentText->setAnchorPoint(ccp(0, 1));
	currentText->setPosition(ccp(250, 125));

	addChild(currentName);
	addChild(currentText);

}

DialogueBox::~DialogueBox(){
	boxImage->release();
}