#ifndef __DS_AAAUDIOCOMMAND__
#define __DS_AAAUDIOCOMMAND__

#include "cocos2d.h"
#include "ATSNode.h"
#include "TileLayer.h"
#include "UILayer.h"
#include "VNLayer.h"

USING_NS_CC;

class DSScript;

class AAAudioCommand : public ATSNode
{
public:
	AAAudioCommand(DSScript* script) : ATSNode(script){
		isBGM = false;
		isStop=false;
	};
	~AAAudioCommand(){};

	void setAudioName(String target){ audioName = target; };
	void setVolume(String target){ volume = atof(target.getCString()); };
	void setIsBGM(){ isBGM = true; };
	void setIsStop(){ isStop = true; };


	void runATS(TileLayer* tileLayer, UILayer* uiLayer, VNLayer* vnLayer) override;
	bool isDone();
private:
	String audioName;
	float volume;
	bool isBGM;
	bool isStop;
};

#endif