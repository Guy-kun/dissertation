#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "TileLayer.h"
#include "ScriptManager.h"
#include "UILayer.h"
#include "VNLayer.h"


#include <iostream>

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include <Windows.h>
#include <Xinput.h>
#pragma comment(lib, "XInput.lib")
#endif

// http://fisnikhasani.com/xbox-360-controller-input-with-c-using-xinput-h-api/
//
// Constants for gamepad buttons
//
#define XINPUT_GAMEPAD_DPAD_UP          0x0001
#define XINPUT_GAMEPAD_DPAD_DOWN        0x0002
#define XINPUT_GAMEPAD_DPAD_LEFT        0x0004
#define XINPUT_GAMEPAD_DPAD_RIGHT       0x0008
#define XINPUT_GAMEPAD_START            0x0010
#define XINPUT_GAMEPAD_BACK             0x0020
#define XINPUT_GAMEPAD_LEFT_THUMB       0x0040
#define XINPUT_GAMEPAD_RIGHT_THUMB      0x0080
#define XINPUT_GAMEPAD_LEFT_SHOULDER    0x0100
#define XINPUT_GAMEPAD_RIGHT_SHOULDER   0x0200
#define XINPUT_GAMEPAD_A                0x1000
#define XINPUT_GAMEPAD_B                0x2000
#define XINPUT_GAMEPAD_X                0x4000
#define XINPUT_GAMEPAD_Y                0x8000

class GameScene : public cocos2d::Scene
{
public:

	virtual bool init();

	static cocos2d::Scene* scene();

	void menuCloseCallback(Object* pSender);
	void update(float delta);
	void visit();

	CREATE_FUNC(GameScene);
	~GameScene();

	void onTouchesEnded(std::vector<CCTouch*> pTouches, cocos2d::CCEvent *pEvent);
	void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	void keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

private:

	TileLayer* tileLayer;
	UILayer* uiLayer;
	VNLayer* vnLayer;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	bool IsXBOXControlConnected();
	XINPUT_STATE GetState();
	void sendInput(InputDirection dir);

	XINPUT_STATE XBOX_CONTROLLER_State;
	InputDirection lastDirection;
#endif
};


#endif 
