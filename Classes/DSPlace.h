#ifndef __DS_PLACE_H__
#define __DS_PLACE_H__

#include "cocos2d.h"

USING_NS_CC;

class DSPlace : public cocos2d::Object
{
public:
	DSPlace() : m_tag(new String()), m_name(new String()), m_mapPath(new String()), m_lighting(new String()), m_npcList(new String()),m_player(new String()), m_tmxMap(nullptr), m_npcs(new Array()), m_spawns(new Dictionary()){};
	~DSPlace();

	bool init();
	bool loadTMXMap();
	void process();
	int getSpawnZ(){ return spawnZ; };
private:
	int spawnZ;

	CC_SYNTHESIZE_RETAIN(String*, m_tag, Tag);
	CC_SYNTHESIZE_RETAIN(String*, m_name, Name);
	CC_SYNTHESIZE_RETAIN(String*, m_mapPath, Path);
	CC_SYNTHESIZE_RETAIN(String*, m_lighting, Lighting);
	CC_SYNTHESIZE_RETAIN(String*, m_npcList, NPCList);
	CC_SYNTHESIZE_RETAIN(String*, m_player, PlayerInfo);

	CC_SYNTHESIZE_RETAIN(TMXTiledMap*, m_tmxMap, TMXMap);
	CC_SYNTHESIZE_RETAIN(Array*, m_npcs, NPCs);
	CC_SYNTHESIZE_RETAIN(Dictionary*, m_spawns, Spawns);

};

#endif 
