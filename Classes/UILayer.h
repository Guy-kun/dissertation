#ifndef __UI_LAYER_H__
#define __UI_LAYER_H__

#include "cocos2d.h"
#include <CCEventKeyboard.h>
#include "DialogueBox.h"
enum UIState{
	isogame,dialogue
};
USING_NS_CC;

class UILayer : public cocos2d::Layer
{
public:
	CREATE_FUNC(UILayer);
	virtual bool init();
	~UILayer();

	static cocos2d::Layer* layer();

	void setStageName(String name);
	void say(String who, String what);
	void hideStageName();

	bool isInDialogueMode(){ return state == dialogue; };

	void enterDialogueMode();
	void leaveDialogueMode();
	void onTouchesEnded(std::vector<CCTouch*> pTouches, cocos2d::CCEvent *pEvent);
private:
	
	CCSprite* bottombar;
	DialogueBox* dialogueBox;
	std::string currentStageName;
	CCLabelTTF* stageLabel;
	UIState state;

};

#endif 
