#ifndef __DS_AACHANGEMAP__
#define __DS_AACHANGEMAP__

#include "cocos2d.h"
#include "ATSNode.h"
#include "TileLayer.h"
#include "UILayer.h"
#include "VNLayer.h"

USING_NS_CC;

class DSScript;

class AAChangeMap : public ATSNode
{
public:
	AAChangeMap(DSScript* script) : ATSNode(script){};
	~AAChangeMap(){};

	void setTargetMap(String target){ targetMap = target; };
	void setTargetSpawn(String target){ targetSpawn = target; };

	String getTargetMap(){ return targetMap; };
	String getTargetSpawn(){ return targetSpawn; };


	void runATS(TileLayer* tileLayer, UILayer* uiLayer, VNLayer* vnLayer) override;
	bool isDone();
private:
	String targetMap;
	String targetSpawn;

};

#endif