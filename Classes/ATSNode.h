#ifndef __ATS_NODE__
#define __ATS_NODE__

#include "cocos2d.h"

class TileLayer;
class DSScript;
class UILayer;
class VNLayer;

USING_NS_CC;

class ATSNode : public cocos2d::Object
{
public:
	ATSNode(DSScript* ownerScript) : shouldWait(false),doesTakeInputControl(false),doesYieldInputControl(false), currentChildIndex(0), parentScript(ownerScript){
		children = nullptr;
		setChildren(Array::create());
	};
	~ATSNode(){};

	//Adds ATS child to the end of current node
	void addChildATS(ATSNode* child){
		children->addObject(child);
	}
	//Adds ATS children from block
	void addChildrenFromATSNode(ATSNode* node){
		for (int i = 0; i < node->getChildren()->count();i++){
			children->addObject(node->getChildren()->objectAtIndex(i));
		}
	}

	/*Overridden by subclasses*/
	//Called every update, children's actions are run if not overriden with functionality
	//Passed references to all layers that could be affected by actions
	virtual void runATS(TileLayer* tileLayer, UILayer* uiLayer,VNLayer* vnLayer);

	bool takesInputControl(){
		return doesTakeInputControl;
	}
	bool yieldsInputControl(){
		return doesYieldInputControl;
	}
	//Cancels ATS in all children
	void finishATS(){
		for (int i = 0; i < children->count(); i++){
			static_cast<ATSNode*>(children->objectAtIndex(i))->finishATS();
		}
	}
	//Resets ATS in all children
	void resetATS(){
		currentChildIndex = 0;
		for (int i = 0; i < children->count(); i++){
			static_cast<ATSNode*>(children->objectAtIndex(i))->resetATS();
		}
	}

	//Returns true if node is finished
	//Default implementation is for checking subnodes, overwritten by subclasses
	virtual bool isDone(){ return currentChildIndex >= children->count(); };

	//Returns externally done state. Typically needs no overload
	virtual bool isExternallyDone(){ return _isExternallyDone; };
	virtual void setIsExternallyDone(bool val){ _isExternallyDone = val; };

	//causing waits is done by with/at/transitions that hold up the game until finished (no text displayed) 
	//The variable is set on a block at creation, and typical structure will be [inherited atl,ATS for showing (this can hold up), ATS declared after :(colon)]
	bool canProgress(){ 
		for (int i = 0; i < children->count(); i++){
			ATSNode* n = static_cast<ATSNode*>(children->objectAtIndex(i));
			if (shouldWait){
				//If the block should cause a wait, return true only when no children are not yet done
				if (!n->isDone())
					return false;
			}
			else{
				//Otherwise check if all children can progress, if they should cause a wait, they'll trigger the above statement inside themselves, blocks with no children will return true
				if (!n->canProgress())
					return false;
			}
		}
		return true;
	};

	//Removes all sprite references
	void cleanup();
	//Sets up current node 
	void setup();

	DSScript* getParentScript(){ return parentScript; };

	//TODO warpers

private:
	CC_SYNTHESIZE_RETAIN(Array*, children, Children);
	//Pointer to Image ATS is working on
	//TODO, sprite
	//Current index of child being run
	int currentChildIndex;
	//Whether block should cause a wait
	bool shouldWait;

protected:
	DSScript* parentScript;
	//Internal 'done' state
	bool _isDone = false;
	//External is used for nodes that require input (default true/unimplemented)
	bool _isExternallyDone = true;

	/*
		Params specify if this node takes or yields input control in scriptmanager
	*/
	bool doesTakeInputControl;
	bool doesYieldInputControl;
};

#endif 
