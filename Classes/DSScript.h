#ifndef __DS_SCRIPT__
#define __DS_SCRIPT__

#include "cocos2d.h"

USING_NS_CC;

class DSScript : public CCObject
{
public:
	DSScript(std::vector<std::string> data, std::string type, std::string tag){
		lines = data;
		currentLine = 0;
		parentTag = tag;
		parentType = type;
	};
	~DSScript(){};

	std::string getNextLine(){
		currentLine++;
		return lines[currentLine-1];
	}
	std::string getParentTag(){ return parentTag; };

	bool isFinished(){
		return currentLine >= lines.size();
	}
	void reset(){
		currentLine = 0;
	}
private:
	std::vector<std::string> lines;
	int currentLine;
	std::string parentType;
	std::string parentTag;

};

#endif