#include "VNLayer.h"

USING_NS_CC;

Layer* VNLayer::layer()
{
	Layer *layer = Layer::create();
	return layer;
}

bool VNLayer::init()
{
	images = std::vector<VNImage*>();
	return true;
}
void VNLayer::removeImage(Node* ignore, VNImage* img){
	for (int i = images.size() - 1; i >= 0; i--){
		if (strcmp(images[i]->getName().getCString(), img->getName().getCString()) == 0 && strcmp(images[i]->getExpression().getCString(), img->getExpression().getCString()) == 0)
		{
			img = images[i];
			images.erase(images.begin() + i);
			break;
		}
	}
	removeChild(img);
	img->release();
}
VNImage* VNLayer::imageWithName(String name){
	std::vector<VNImage*> duplicates = std::vector<VNImage*>();
	VNImage* i = nullptr;
	for (VNImage* v : images)
	{
		if (strcmp(v->getName().getCString(), name.getCString()) == 0)
		{
			//Instantly remove any but last occurence of this image, they are accidental duplicates (should be none)
			if (i != nullptr)
				duplicates.push_back(i);
			i = v;
		}
	}
	//Cleanup if needed
	for (int j = duplicates.size() - 1; j >= 0;j--)
	{
		removeImage(nullptr,duplicates[j]);
	}
	return i;
}

void VNLayer::addFadeOutDissolve(VNImage* image){
	CCFadeOut* fade = CCFadeOut::create(0.3 * 3 / 2);
	CCCallFuncND* removeCallback = CCCallFuncND::create(this, (cocos2d::SEL_CallFuncND) &VNLayer::removeImage, (void*)image);

	image->runAction(CCSequence::createWithTwoActions(fade,removeCallback));
}

void VNLayer::addFadeInDissolve(VNImage* image){
	CCFadeIn* fade = CCFadeIn::create(0.3 * 2 / 3);

	image->runAction(fade);
}

void VNLayer::showImage(String name, String expression, String with, String at){
	Size winSize = CCEGLView::sharedOpenGLView()->getFrameSize();

	VNImage* newImage = VNImage::create(name, expression);
	//Find old image with same name (can be different expression)
	VNImage* oldImage = imageWithName(name);
	
	//Default position to left
	if (at.length() == 0)
		at = String("left");

	if (strcmp(at.getCString(), "left")==0)
	{
		newImage->setAnchorPoint(ccp(0, newImage->getAnchorPoint().y));
		newImage->setPosition(ccp(0, newImage->getPositionY()));
	}
	else if (strcmp(at.getCString(), "right") == 0)
	{
		newImage->setAnchorPoint(ccp(1, newImage->getAnchorPoint().y));
		newImage->setPosition(ccp(winSize.width, newImage->getPositionY()));
	}
	else if (strcmp(at.getCString(), "center") == 0)
	{
		newImage->setAnchorPoint(ccp(0.5, newImage->getAnchorPoint().y));
		newImage->setPosition(ccp(winSize.width / 2, newImage->getPositionY()));
	}

	//Default 'with' is nothing which allows image to instantly appear
	if (with.length() == 0)
	{
		//Remove old image instantly
		if (oldImage != nullptr)
		{
			removeImage(nullptr, oldImage);
		}
	}
	else
	{
		//Transition, only 'dissolve' supported for now
		if (strcmp(with.getCString(), "dissolve") == 0){
			if (oldImage != nullptr)
				addFadeOutDissolve(oldImage);
			newImage->setOpacity(0);
			addFadeInDissolve(newImage);
		}
	}
	
	newImage->retain();
	images.push_back(newImage);
	addChild(newImage);
}

void VNLayer::hideImage(String name, String with)
{
	VNImage* img = imageWithName(name);
	if (img != nullptr)
	{
		if (with.length() == 0)
		{
			//Remove image instantly
			removeImage(nullptr, img);
		}
		else if (strcmp(with.getCString(), "dissolve") == 0){
			addFadeOutDissolve(img);
		}

	}
}

VNLayer::~VNLayer(){
}
