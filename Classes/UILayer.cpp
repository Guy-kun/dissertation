#include "UILayer.h"

USING_NS_CC;

Layer* UILayer::layer()
{
	Layer *layer = Layer::create();
	return layer;
}

bool UILayer::init()
{
	currentStageName = "";
	state = isogame;
	stageLabel = nullptr;
	auto listener = EventListenerTouchAllAtOnce::create();
	listener->onTouchesEnded = CC_CALLBACK_2(UILayer::onTouchesEnded, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);

	//Set up bottom bar
	bottombar = CCSprite::create();
	bottombar->initWithFile("Images/screenbottom.png");
	addChild(bottombar);
	bottombar->setPosition(ccp(0, 0));
	bottombar->setAnchorPoint(ccp(0, 0));

	//Set up dialogue box
	dialogueBox = DialogueBox::create();
	addChild(dialogueBox);

	return true;
}

void UILayer::hideStageName(){
	if (stageLabel != nullptr)
	{
		removeChild(stageLabel);
		stageLabel == nullptr;
	}
}

void UILayer::setStageName(String name){
	if (stageLabel != nullptr)
		removeChild(stageLabel);

	currentStageName = std::string(name.getCString());

	if (state == isogame)
	{
		stageLabel = CCLabelTTF::create(name.getCString(), "Helvetica", 30,
			Size(790, 50), kCCTextAlignmentRight,
			kCCVerticalTextAlignmentBottom);
		//stageLabel->setColor(ccc3(17, 76, 93));
		stageLabel->setAnchorPoint(ccp(0, 0));
		stageLabel->setPosition(ccp(0, 3));
		addChild(stageLabel);
	}

}
void UILayer::say(String who, String what){
	if (state == dialogue)
	{
		dialogueBox->say(who, what);
	}
}
void UILayer::enterDialogueMode(){
	if (state == isogame)
	{
		//Enter dialogue state
		state = dialogue;
		hideStageName();
		dialogueBox->enterDialogueMode();
	}
}
void UILayer::leaveDialogueMode(){
	if (state == dialogue)
	{
		//Leave dialogue state
		state = isogame;
		dialogueBox->leaveDialogueMode();
		setStageName(currentStageName);
	}
}

void UILayer::onTouchesEnded(std::vector<CCTouch*> pTouches, cocos2d::CCEvent *pEvent) {

}

UILayer::~UILayer(){
}
