#include "Parser.h"
#include <regex>

const std::regex regex("[^\\s\"']+|\"([^\"]*)\"|'([^']*)'");
bool isEqual(std::string one, const char* two){
	return strcmp(one.c_str(), two) == 0;
}

//http://stackoverflow.com/questions/7378902/how-to-efficiently-remove-double-quotes-from-stdstring-if-they-exist
std::string removeQuotes(std::string in){
	if (in.front() == '"') {
		in.erase(0, 1); 
		in.erase(in.size() - 1); 
	}
	return in;
}

ATSNode* Parser::getNextNode(DSScript* script){
	while (!script->isFinished())
	{
		std::string currentLine = script->getNextLine();
		std::vector<std::string> lineWords = std::vector<std::string>();

		const std::sregex_token_iterator end;
		for (std::sregex_token_iterator i(currentLine.begin(),
			currentLine.end(), regex);
			i != end;
		++i)
		{
			lineWords.push_back(*i);
		}

		if (lineWords.size() > 0)
		{
			//Check first keyword

			/*
				UI Commands
				*/
			if (isEqual(lineWords[0], "setstagetitle"))
			{
				if (lineWords.size() < 2)
					return nullptr;
				//Set stage name
				AAUICommand* c = new AAUICommand(script);
				c->setIsStageNameChange(true);
				c->setparam1(removeQuotes(lineWords[1]));
				return c;
			}
			else if (isEqual(lineWords[0], "enterdialoguemode")){
				//Enter dialogue mode
				AAUICommand* c = new AAUICommand(script);
				c->setIsEnterDialogueMode(true);
				return c;
			}
			else if (isEqual(lineWords[0], "leavedialoguemode")){
				//Enter dialogue mode
				AAUICommand* c = new AAUICommand(script);
				c->setIsLeaveDialogueMode(true);
				return c;
			}

			/*
				Text Commands
				*/
			else if (isEqual(lineWords[0], "say")){
				if (lineWords.size() < 3)
					return nullptr;
				//Dialogue
				AASay* c = new AASay(script);
				c->setWho(removeQuotes(lineWords[1]));
				c->setWhat(lineWords[2]);
				return c;
			}

			/*
				Image Commands
				*/
			else if (isEqual(lineWords[0], "show") || isEqual(lineWords[0], "hide")){
				bool isHide = isEqual(lineWords[0], "hide");
				if (lineWords.size() < (isHide?2:3))
					return nullptr;

				AAShowHide* c = new AAShowHide(script);
				if (isHide)
					c->setisHide();
				c->setName(lineWords[1]);
				if (!isHide)
					c->setExpression(lineWords[2]);
				

				bool lastWasAt = false;
				bool lastWasWith = false;
				//Check remaining parameters
				for (int i = (isHide ? 2 : 3); i < lineWords.size(); i++)
				{
					if (!lastWasAt && !lastWasWith)
					{
						if (isEqual(lineWords[i], "at"))
							lastWasAt = true;
						if (isEqual(lineWords[i], "with"))
							lastWasWith = true;
					}
					else
					{
						if (lastWasAt)
						{
							c->setAt(lineWords[i]);
							lastWasAt = false;
						}
						else if (lastWasWith)
						{
							c->setWith(lineWords[i]);
							lastWasWith = false;
						}
					}
				}

				return c;
			}
			/*
				Map Commands
			*/
			else if (isEqual(lineWords[0], "map")){
				if (lineWords.size() < 3)
					return nullptr;
				//Map event
				if (isEqual(lineWords[1], "delete"))
				{
					AAMapEvent* c = new AAMapEvent(script);
					c->setIsDelete();
					c->setTargetObject(String(lineWords[2]));
					return c;
				}
			}
			else if (isEqual(lineWords[0], "changemap")){
				if (lineWords.size() < 4)
					return nullptr;
				//Change map
					AAChangeMap* c = new AAChangeMap(script);
					c->setTargetMap(String(lineWords[1]));

					bool lastWasAt = false;
					//Check remaining parameters
					for (int i = 2; i < lineWords.size(); i++)
					{
						if (!lastWasAt)
						{
							if (isEqual(lineWords[i], "at"))
								lastWasAt = true;
						}
						else
						{
							if (lastWasAt)
							{
								c->setTargetSpawn(lineWords[i]);
								lastWasAt = false;
							}
						}
					}

					return c;
			}
			else if (isEqual(lineWords[0], "changelighting")){
				if (lineWords.size() < 2)
					return nullptr;
				//Lighting event
				AALightingCommand* c = new AALightingCommand(script);
					c->setIsChangeShader();
					c->setShaderName(String(lineWords[1]));
					return c;
			}
			else if (isEqual(lineWords[0], "npc")){
				if (lineWords.size() < 2)
					return nullptr;
				if (isEqual(lineWords[1], "move"))
				{
					if (lineWords.size() < 5)
						return nullptr;
					AANPCCommand* c = new AANPCCommand(script);
					c->setIsMoveNPC();
					c->setTargetNPC(String(lineWords[2]));
					c->setTargetDirection(String(lineWords[3]));
					c->setTargetAmount(String(lineWords[4]));
					return c;
				}

				//Lighting event
				AALightingCommand* c = new AALightingCommand(script);
				c->setIsChangeShader();
				c->setShaderName(String(lineWords[1]));
				return c;
			}

			else if (isEqual(lineWords[0], "music")){
				if (lineWords.size() < 5)
					return nullptr;
				//Music event
				AAAudioCommand* c = new AAAudioCommand(script);
				if (isEqual(lineWords[1], "bgm")){
					c->setIsBGM();
				}
				if (isEqual(lineWords[2], "stop")){
					c->setIsStop();
				}
				c->setAudioName(String(lineWords[3]));
				c->setVolume(String(lineWords[4]));

				return c;
			}
			//Unprocessed, keep going
		}
	}
	//Script came to an end
	return nullptr;
}


