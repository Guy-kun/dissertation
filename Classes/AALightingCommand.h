#ifndef __DS_AALIGHTINGCOMMAND__
#define __DS_AALIGHTINGCOMMAND__

#include "cocos2d.h"
#include "ATSNode.h"
#include "TileLayer.h"
#include "UILayer.h"
#include "VNLayer.h"

USING_NS_CC;

class DSScript;

class AALightingCommand : public ATSNode
{
public:
	AALightingCommand(DSScript* script) : ATSNode(script){ _isChangeShader = false; };
	~AALightingCommand(){};

	void setShaderName(String target){ targetShader = target; };
	void setIsChangeShader(){ _isChangeShader = true; };

	String getShaderName(){ return targetShader; };
	bool isChangeShader(){ return _isChangeShader; };

	void runATS(TileLayer* tileLayer, UILayer* uiLayer, VNLayer* vnLayer) override{
		if (!_isDone)
		{
			if (_isChangeShader)
				tileLayer->setShader(targetShader);
			_isDone = true;
		}
	};
	bool isDone(){
		return _isDone;
	};
private:
	String targetShader;
	bool _isChangeShader;

};

#endif