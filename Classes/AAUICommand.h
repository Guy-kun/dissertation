#ifndef __DS_AAUICOM__
#define __DS_AAUICOM__

#include "cocos2d.h"
#include "ATSNode.h"
#include "TileLayer.h"
#include "UILayer.h"
USING_NS_CC;

class DSScript;

class AAUICommand : public ATSNode
{
public:
	AAUICommand(DSScript* script) : ATSNode(script){
		isStagenameChange = false;
		isEnterDialogueMode = false;
		isLeaveDialogueMode = false;
	};
	~AAUICommand(){
	};


	void setIsStageNameChange(bool w){ isStagenameChange = w; };
	void setIsEnterDialogueMode(bool w){
		doesTakeInputControl = true;
		isEnterDialogueMode = w; 
	};
	void setIsLeaveDialogueMode(bool w){
		doesYieldInputControl = true;
		isLeaveDialogueMode = w; 
	};

	void setparam1(String w){ param1 = w; };

	void runATS(TileLayer* tileLayer, UILayer* uiLayer, VNLayer* vnLayer)override{
		if (isStagenameChange)
			uiLayer->setStageName(param1);
		else if (isEnterDialogueMode)
			uiLayer->enterDialogueMode();
		else if (isLeaveDialogueMode)
			uiLayer->leaveDialogueMode();
		_isDone = true;
	}
	bool isDone() override{ return _isDone; };

private:
	bool isStagenameChange;
	bool isEnterDialogueMode;
	bool isLeaveDialogueMode;

	String param1;
};

#endif