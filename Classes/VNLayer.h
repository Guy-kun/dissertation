#ifndef __VN_LAYER_H__
#define __VN_LAYER_H__

#include "cocos2d.h"
#include "VNImage.h"
USING_NS_CC;

class VNLayer : public cocos2d::Layer
{
public:
	CREATE_FUNC(VNLayer);
	virtual bool init();
	~VNLayer();

	static cocos2d::Layer* layer();

	void showImage(String name, String expression, String with,String at);
	void hideImage(String name, String with);

private:
	std::vector<VNImage*> images;

	VNImage* imageWithName(String name);
	void removeImage(Node* ignore, VNImage* img);

	void addFadeOutDissolve(VNImage* image);
	void addFadeInDissolve(VNImage* image);
};

#endif 
