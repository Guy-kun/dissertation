#ifndef __DALOGUEBOX__
#define __DALOGUEBOX__

#include "cocos2d.h"

USING_NS_CC;

class DialogueBox : public cocos2d::Layer
{
public:
	CREATE_FUNC(DialogueBox);
	virtual bool init();
	~DialogueBox();

	static cocos2d::Layer* layer();

	void enterDialogueMode();
	void leaveDialogueMode();
	void say(String character, String text);

private:
	bool isInDialogueMode;
	CCSprite* boxImage;

	CCLabelTTF* currentText;
	CCLabelTTF* currentName;

	void cleanupText();
};

#endif