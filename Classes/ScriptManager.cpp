#include "ScriptManager.h"

void ScriptManager::update(float delta, TileLayer* tileLayer, UILayer* uiLayer, VNLayer* vnLayer){
	for (ATSNode* node : activeNodes)
	{
		node->runATS(tileLayer,uiLayer,vnLayer);
	}

	//For each running script, check if there is a currently running node that prevents it from progressing
	for (int s = activeScripts.size() - 1; s >= 0;s--){
		DSScript* script = activeScripts[s];

		bool parseNext = true;
		for (int i = activeNodes.size() - 1; i >= 0;i--){
			ATSNode* node = activeNodes[i];
			if (node->getParentScript() == script)
			{
				//ATSNode belonging to this script
				if (!node->canProgress() || !node->isExternallyDone())
				{
					parseNext = false;
				}
			}
			//Remove finished nodes
			if (node->isDone() && node->isExternallyDone())
			{
				activeNodes.erase(activeNodes.begin() + i);
			}
		}

		if (parseNext){
			ATSNode* n = getNextNode(script);
			if (n == nullptr)
			{
				//Script is finished, reset it in-case it needs to be run again
				script->reset();
				activeScripts.erase(activeScripts.begin()+s);
			}
			else
			{
				//If this node takes or yields control, add it to the awaiting list
				//Last entry gets input first
				if (n->takesInputControl())
					scriptsAwaitingInput.push_back(script);
				else if (n->yieldsInputControl())
				{
					auto it = std::find(scriptsAwaitingInput.begin(), scriptsAwaitingInput.end(), script);
					if (it != scriptsAwaitingInput.end())
						scriptsAwaitingInput.erase(it);
				}

				activeNodes.push_back(n);
			}
		}

	}
}

ATSNode* ScriptManager::getNextNode(DSScript* script){
	return parser->getNextNode(script);
}

/*
	AASay statements are typically ones that cause waits that require input
	I.E. Scripts in the 'awaiting' list, receive input and allow active AASay to become ' externally finished' allowing progress
*/
void ScriptManager::processInput(){
	for (int i = scriptsAwaitingInput.size() - 1; i >= 0; i--)
	{
		DSScript* s = scriptsAwaitingInput[i];
		for (ATSNode* node : activeNodes)
		{
			if (node->getParentScript() == s)
				node->setIsExternallyDone(true);
		}
	}
}