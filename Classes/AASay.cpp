#include "AASay.h"

void AASay::runATS(TileLayer* tileLayer, UILayer* uiLayer, VNLayer* vnLayer){
	if (!_isDone)
	{
		uiLayer->say(who, what);
		_isDone = true;
	}
}

bool AASay::isDone(){
	return _isDone;
}