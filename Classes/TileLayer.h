#ifndef __TILE_LAYER_H__
#define __TILE_LAYER_H__

#include "cocos2d.h"
#include "DSPlace.h"
#include "Player.h"
#include <CCEventKeyboard.h>
#include "SimpleAudioEngine.h"  


USING_NS_CC;

enum InputDirection{
	UP,DOWN,LEFT,RIGHT,NONE
};

class TileLayer : public cocos2d::Layer
{
public:
	CREATE_FUNC(TileLayer);
	virtual bool init();
	~TileLayer();

	static cocos2d::Layer* layer();

	//if spawnName is null, 'default' will be used
	void makeMapCurrent(String* tag, String* spawnName);
	void setViewPointCenter(CCPoint position);
	void visit();

	void playerInteractInFacingDirection();

	void deleteTileAtObjectNamed(String obj);
	void setShader(String name);
	void moveNPC(String name, Direction direction, int amount);

	void playBGM(String name,float volume);
	void stopBGM();
	void playSFX(String name,float volume);

	void onTouchesEnded(std::vector<CCTouch*> pTouches, cocos2d::CCEvent *pEvent);
	void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	void keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	void processInput(InputDirection dir);

private:

	DSPlace* loadDSPlace(DSPlace* place);
	Player* player;
	std::vector<NPC*>* npcs;


	DSPlace* m_currentPlace; 
	//Strings of DSPlaces by tag
	CCDictionary* m_dsPlaces;
	CCDictionary* shaderPrograms;


	RenderTexture* screenRenderTex;

	CCPoint spawnForName(String* spawnName, DSPlace* place);
	void loadPlayer(String* playerInfo, CCPoint position, String* spawnPoint);
	CCPoint isoCoordsForRealPosition(CCPoint position);
	CCPoint realCoordsForIsoPosition(CCPoint position);

	bool isTileAtIsoCoordBlocked(CCPoint coord);
	bool canNPCMoveInDirection(NPC* npc, Direction dir);


	void loadShaderPrograms();

};

#endif 
