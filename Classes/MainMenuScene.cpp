#include "MainMenuScene.h"
#include "GameScene.h"
USING_NS_CC;

Scene* MainMenuScene::scene()
{
	// 'scene' is an autorelease object
	MainMenuScene *scene = MainMenuScene::create();
	// return the scene
	return scene;
}

void startGame(){
	Scene* game = GameScene::create();
	CCDirector::sharedDirector()->replaceScene(CCTransitionCrossFade::create(0.5, game));
}

bool MainMenuScene::init()
{
	startScreen = CCSprite::create();
	startScreen->initWithFile("Images/startscreen.png");
	startScreen->setAnchorPoint(ccp(0, 0));
	addChild(startScreen);

	auto listener = EventListenerTouchAllAtOnce::create();
	listener->onTouchesEnded = CC_CALLBACK_2(MainMenuScene::onTouchesEnded, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);

	auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(MainMenuScene::keyPressed, this);
	keyboardListener->onKeyReleased = CC_CALLBACK_2(MainMenuScene::keyReleased, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);
	return true;
}

void MainMenuScene::onTouchesEnded(std::vector<CCTouch*> pTouches, cocos2d::CCEvent *pEvent) {
	startGame();
}

void MainMenuScene::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{

}
void MainMenuScene::keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event){
	startGame();
}

void MainMenuScene::menuCloseCallback(Object* pSender)
{
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

MainMenuScene::~MainMenuScene(){

}
