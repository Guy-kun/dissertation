#ifndef __DS_ScriptManager__
#define __DS_ScriptManager__

#include "cocos2d.h"
#include "ATSNode.h"
#include "DSScript.h"
#include "Parser.h"
#include "TileLayer.h"
#include<sstream>

USING_NS_CC;

class ScriptManager
{
public:
	static ScriptManager& getInstance()
	{
		static ScriptManager instance; 
		// Guaranteed to be destroyed.
		// Instantiated on first use.
		return instance;
	}

	void addScript(DSScript* script){
		activeScripts.push_back(script);
	};

	void addSingleATSCommandAsScript(std::string command){
		std::vector<std::string> lines = std::vector<std::string>();
		std::stringstream ss(command);
		std::string item;
		while (getline(ss, item, '\n')) {
			lines.push_back(item);
		}

		DSScript* script = new DSScript(lines, std::string(""), std::string(""));
		activeScripts.push_back(script);
	}

	void update(float delta, TileLayer* tileLayer, UILayer* uiLayer,VNLayer* vnLayer);
	void processInput();

	bool isScriptAwaitingInput(){
		return scriptsAwaitingInput.size() > 0;
	}

private:
	std::vector<DSScript*> activeScripts;
	std::vector<ATSNode*> activeNodes;

	/*
		Vector of scripts waiting for input to proceed
		Typically, should only be one at a time, but support for more
		All input will be passed to these scripts until the list is empty

		Used as a global reference if input should come here or to relevant layers
		(I.E. input during dialogue mode)
	*/
	std::vector<DSScript*> scriptsAwaitingInput;

	Parser* parser;

	ATSNode* getNextNode(DSScript* script);

	ScriptManager() {
		activeScripts = std::vector<DSScript*>();
		scriptsAwaitingInput = std::vector<DSScript*>();

		parser = new Parser();
	};
	ScriptManager(ScriptManager const&);  // Don't Implement
	void operator=(ScriptManager const&); // Don't implement
};

#endif