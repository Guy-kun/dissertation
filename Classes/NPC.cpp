#include "NPC.h"

USING_NS_CC;


NPC::NPC(DSScript* init, DSScript* interact, DSScript* main, String* npcName, String* npcTag, String* displayname, String* expr, String* spawnpoint){
	//Set to null to stop CC_SYNTH exploding
	initScript = nullptr;
	interactScript = nullptr;
	mainScript = nullptr;
	neSprite = nullptr;
	nwSprite = nullptr;
	seSprite = nullptr;
	swSprite = nullptr;

	npctag = nullptr;
	name = nullptr;
	displayName = nullptr;
	expression = nullptr;
	spawn = nullptr;


	setInitScript(init);
	setInteractScript(interact);
	setMainScript(main);
	currentDirection = SW;

	setName(npcName);
	setNPCTag(npcTag);
	setDisplayName(displayname);
	setSpawn(spawnpoint);
	loadExpression(expr);
}

Sprite * NPC::loadSprite(String filename){
	Sprite* s = new Sprite();
	s->initWithFile(filename.getCString());
	s->getTexture()->setAliasTexParameters();
	s->setAnchorPoint(ccp(0.5, 0));
	return s;
}
void NPC::loadExpression(String* expression){
	setExpression(expression);
	if (neSprite != nullptr)
	{
		CC_SAFE_RELEASE(neSprite);
		CC_SAFE_RELEASE(nwSprite);
		CC_SAFE_RELEASE(swSprite);
		CC_SAFE_RELEASE(seSprite);
	}

	std::stringstream ss;
	ss << "Sprites/" << getName()->getCString() << "/" << getExpression()->getCString() << "_NE.png";
	setNESprite(loadSprite(ss.str().c_str()));
	ss.str(std::string());
	ss.clear();
	ss << "Sprites/" << getName()->getCString() << "/" << getExpression()->getCString() << "_NW.png";
	setNWSprite(loadSprite(ss.str().c_str()));
	ss.str(std::string());
	ss.clear();
	ss << "Sprites/" << getName()->getCString() << "/" << getExpression()->getCString() << "_SE.png";
	setSESprite(loadSprite(ss.str().c_str())); ss.clear();
	ss.str(std::string());
	ss.clear();
	ss << "Sprites/" << getName()->getCString() << "/" << getExpression()->getCString() << "_SW.png";
	setSWSprite(loadSprite(ss.str().c_str()));

	neSprite->retain();
	nwSprite->retain();
	seSprite->retain();
	swSprite->retain();

}

bool NPC::checkMovementBounds(Size mapSize, Size tileSize, Point newPos){
	if (newPos.x <= (mapSize.width * tileSize.width) &&
		newPos.y <= (mapSize.height * tileSize.height) &&
		newPos.y >= 0 &&
		newPos.x >= 0)
		return true;
	return false;
}

void NPC::moveNE(Size mapSize, Size tileSize){
	if (currentDirection == NE)
	{
		Point p = getPosition();
		p.x += tileSize.width / 2;
		p.y += tileSize.height / 2;
		if (checkMovementBounds(mapSize, tileSize, p))
		{
			//Within bounds
			setPosition(p);
		}
	}
	else
		turn(NE);
}

void NPC::moveNW(Size mapSize, Size tileSize){
	if (currentDirection == NW)
	{
		Point p = getPosition();
		p.x -= tileSize.width / 2;
		p.y += tileSize.height / 2;
		if (checkMovementBounds(mapSize, tileSize, p))
		{
			//Within bounds
			setPosition(p);
		}
	}
	else
		turn(NW);
}

void NPC::moveSE(Size mapSize, Size tileSize){
	if (currentDirection == SE)
	{
		Point p = getPosition();
		p.x += tileSize.width / 2;
		p.y -= tileSize.height / 2;
		if (checkMovementBounds(mapSize, tileSize, p))
		{
			//Within bounds
			setPosition(p);
		}
	}
	else
		turn(SE);
}

void NPC::moveSW(Size mapSize, Size tileSize){
	if (currentDirection == SW)
	{
		Point p = getPosition();
		p.x -= tileSize.width / 2;
		p.y -= tileSize.height / 2;
		if (checkMovementBounds(mapSize, tileSize, p))
		{
			//Within bounds
			setPosition(p);
		}
	}
	else
		turn(SW);
}

void NPC::setNewDirection(Direction dir){
	currentDirection = dir;
}

void NPC::visit(){
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, .5f);
	//Add current sprite as a child, so that it takes on properties of this node
	//(zorder etc matters for the map)
	addChild(getCurrentSprite());
	Node::visit();
	removeAllChildren();
	glDisable(GL_ALPHA_TEST);
}

NPC::~NPC(){
	CC_SAFE_RELEASE(initScript);
	CC_SAFE_RELEASE(interactScript);
	CC_SAFE_RELEASE(mainScript);
	CC_SAFE_RELEASE(neSprite);
	CC_SAFE_RELEASE(nwSprite);
	CC_SAFE_RELEASE(swSprite);
	CC_SAFE_RELEASE(seSprite);
}