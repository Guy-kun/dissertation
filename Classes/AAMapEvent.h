#ifndef __DS_AAMAPEVENT__
#define __DS_AAMAPEVENT__

#include "cocos2d.h"
#include "ATSNode.h"
#include "TileLayer.h"
#include "UILayer.h"
#include "VNLayer.h"

USING_NS_CC;

class DSScript;

class AAMapEvent : public ATSNode
{
public:
	AAMapEvent(DSScript* script) : ATSNode(script){};
	~AAMapEvent(){};

	void setIsDelete(){ _isDelete = true; };
	void setTargetObject(String target){ targetObject = target; };

	bool isDelete(){ return _isDelete; };
	String getTargetObject(){ return targetObject; }; 

	void runATS(TileLayer* tileLayer, UILayer* uiLayer, VNLayer* vnLayer) override;
	bool isDone();
private:
	bool _isDelete;
	String targetObject;
};

#endif