#include "GameScene.h"
USING_NS_CC;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)

bool GameScene::IsXBOXControlConnected()
{
	//Invoke the memset(); function to zero the XBOX_CONTROLLER_State. 
	memset(&XBOX_CONTROLLER_State, 0, sizeof(XINPUT_STATE));

	//We store the XInputGetState value inside result, note that result is a DWORD which is a typedef unsigned long. 
	DWORD result = XInputGetState(0, &XBOX_CONTROLLER_State);

	//Check if the controller is disconnected using the Ternary Operator. 
	return  result == ERROR_DEVICE_NOT_CONNECTED ? false : true;
}

XINPUT_STATE GameScene::GetState()
{
	memset(&XBOX_CONTROLLER_State, 0, sizeof(XINPUT_STATE));
	XInputGetState(0, &XBOX_CONTROLLER_State);
	return XBOX_CONTROLLER_State;
}

#endif
Scene* GameScene::scene()
{
	// 'scene' is an autorelease object
	GameScene *scene = GameScene::create();
	// return the scene
	return scene;
}
bool GameScene::init()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	lastDirection = NONE;
#endif
	tileLayer = TileLayer::create();
	tileLayer->retain();
	addChild(tileLayer);

	vnLayer = VNLayer::create();
	vnLayer->retain();
	addChild(vnLayer);

	uiLayer = UILayer::create();
	uiLayer->retain();
	addChild(uiLayer);

	scheduleUpdate();

	auto listener = EventListenerTouchAllAtOnce::create();
	listener->onTouchesEnded = CC_CALLBACK_2(GameScene::onTouchesEnded, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);

	auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(GameScene::keyPressed, this);
	keyboardListener->onKeyReleased = CC_CALLBACK_2(GameScene::keyReleased, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);
	return true;
}

void GameScene::update(float delta){
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	if (IsXBOXControlConnected()){
		if (GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT)
		{
			lastDirection = LEFT;
		}
		else if (GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT)
		{
			lastDirection = RIGHT;
		}
		else if (GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP)
		{
			lastDirection = UP;
		}
		else if (GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN)
		{
			lastDirection = DOWN;
		}
		else
		{
			//No direction pressed
			if (lastDirection != NONE){
				if (ScriptManager::getInstance().isScriptAwaitingInput())
					ScriptManager::getInstance().processInput();
				else
					tileLayer->processInput(lastDirection);
				lastDirection = NONE;
			}
		}
	}
#endif
	ScriptManager::getInstance().update(delta,tileLayer,uiLayer,vnLayer);

}

void GameScene::visit(){
	tileLayer->visit();
	if (uiLayer->isInDialogueMode())
	{
		uiLayer->visit();
		vnLayer->visit();
	}
	else
	{
		vnLayer->visit();
		uiLayer->visit();
	}
}


void GameScene::onTouchesEnded(std::vector<CCTouch*> pTouches, cocos2d::CCEvent *pEvent) {
	ScriptManager::getInstance().processInput();
}

void GameScene::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{

}
void GameScene::keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event){
	ScriptManager::getInstance().processInput();
}

void GameScene::menuCloseCallback(Object* pSender)
{
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

GameScene::~GameScene(){
	tileLayer->release();
	uiLayer->release();
}
