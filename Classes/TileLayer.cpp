#include "TileLayer.h"
#include "FileReader.h"
#include "NPCParser.h"
#include "ScriptManager.h"

USING_NS_CC;

Layer* TileLayer::layer()
{
	Layer *layer = Layer::create();
	return layer;
}

bool TileLayer::init()
{
	Size winSize = CCEGLView::sharedOpenGLView()->getFrameSize();
	screenRenderTex = RenderTexture::create(winSize.width, winSize.height);
	screenRenderTex->setPosition(ccp(winSize.width / 2, winSize.height / 2));
	screenRenderTex->retain();
	loadShaderPrograms();

	this->setTouchEnabled(true);
	player = nullptr;
	m_currentPlace = nullptr;
	m_dsPlaces = CCDictionary::create();
	m_dsPlaces->retain();
	//Process DSPlaces file
	vector<string> lines = FileReader::ReadFile("Scripts/DSPlaces", 'r');
	DSPlace* currProcessingPlace = nullptr;

	for (string line : lines){
		if (line.length() >= 2 && strcmp(line.substr(0, 2).c_str(), "\r") != 0){
			if (strcmp(line.substr(0, 1).c_str(), "#") != 0)
			{
				//Process line
				if (currProcessingPlace == nullptr){
					currProcessingPlace = new DSPlace();
					currProcessingPlace->retain();
				}

				if (line.length() > 5 && strcmp(line.substr(0, 4).c_str(), "Tag:") == 0){
					string param = line.substr(5);
					param.erase(param.find_last_not_of(" \n\r\t") + 1);
					currProcessingPlace->setTag(new String(param));
				}

				if (line.length() > 6 && strcmp(line.substr(0, 5).c_str(), "Name:") == 0){
					string param = line.substr(6);
					param.erase(param.find_last_not_of(" \n\r\t") + 1);
					currProcessingPlace->setName(new String(param));
				}

				if (line.length() > 5 && strcmp(line.substr(0, 4).c_str(), "Map:") == 0){
					string param = line.substr(5);
					param.erase(param.find_last_not_of(" \n\r\t") + 1);
					currProcessingPlace->setPath(new String(param));
				}

				if (line.length() > 9 && strcmp(line.substr(0, 9).c_str(), "Lighting:") == 0){
					string param = line.substr(10);
					param.erase(param.find_last_not_of(" \n\r\t") + 1);
					currProcessingPlace->setLighting(new String(param));
				}

				if (line.length() > 8 && strcmp(line.substr(0, 7).c_str(), "Player:") == 0){
					string param = line.substr(8);
					param.erase(param.find_last_not_of(" \n\r\t") + 1);
					currProcessingPlace->setPlayerInfo(new String(param));
				}

				if (line.length() > 5 && strcmp(line.substr(0, 4).c_str(), "NPC:") == 0){
					string param = line.substr(5);
					param.erase(param.find_last_not_of(" \n\r\t") + 1);
					currProcessingPlace->setNPCList(new String(param));
				}

			}
		}
		else
		{
			//Empty line
			if (currProcessingPlace != nullptr){
				m_dsPlaces->setObject(currProcessingPlace, currProcessingPlace->getTag()->getCString());
				currProcessingPlace = nullptr;
			}
		}
	}

	auto listener = EventListenerTouchAllAtOnce::create();
	listener->onTouchesEnded = CC_CALLBACK_2(TileLayer::onTouchesEnded, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
	
	auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(TileLayer::keyPressed, this);
	keyboardListener->onKeyReleased = CC_CALLBACK_2(TileLayer::keyReleased, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);

	// DEBUG
	makeMapCurrent(new String("w1-1"),nullptr);

	return true;
}

//Override visit
void TileLayer::visit(){
	for (NPC* n : *npcs){
		//Update z order
		Point p = n->getPosition();
		n->setVertexZ(isoCoordsForRealPosition(p).y);
	}

	//Begin rendertexture
	screenRenderTex->beginWithClear(0.0f, 0.0f, 0.0f, 1.0f);
	Layer::visit();
	screenRenderTex->end();
	screenRenderTex->visit();
}

DSPlace* TileLayer::loadDSPlace(DSPlace* place){
	if (!place->loadTMXMap())
	{
		//TMX Map loading failed
		CCLOG("%s %s", "FATAL! Failed to load TMXMap:", place->getPath()->getCString());
	}
	else
	{
		place->process();
	}
	return place;
}

void TileLayer::makeMapCurrent(String* tag,String* spawnName){
	if (m_currentPlace != nullptr){
		TMXTiledMap* m = m_currentPlace->getTMXMap();
		if (m != nullptr);
		//remove all children (map and NPCs)
		removeAllChildrenWithCleanup(true);
		CC_SAFE_RELEASE(m_currentPlace);
	}

	DSPlace* place = static_cast<DSPlace*>(m_dsPlaces->objectForKey(tag->getCString()));
	if (place == NULL)
	{
		CCLOG("%s %s", "FATAL! DSPlace not found:", tag);
	}
	else
	{
		
		//Load succeeded
		m_currentPlace = loadDSPlace(place);
		m_currentPlace->retain();
		addChild(m_currentPlace->getTMXMap());

		//Set place name
		ScriptManager::getInstance().addSingleATSCommandAsScript(string("setstagetitle \"") + string(m_currentPlace->getName()->getCString() + string("\"")));

		//Load NPCs
		npcs = parseNPCsFromNPCString(m_currentPlace->getNPCList());
		for (NPC* n : *npcs){
			m_currentPlace->getTMXMap()->addChild(n, m_currentPlace->getSpawnZ());
			n->setPosition(spawnForName(n->getSpawn(), m_currentPlace));
			//If NPC has init script, add it to the script manager to run here
			if (n->getInitScript() != nullptr)
				ScriptManager::getInstance().addScript(n->getInitScript());
		}

		//Player spawn (can be passed as param)
		if (spawnName == nullptr)
			spawnName = new String("default");
		CCPoint spawnPoint = spawnForName(spawnName, m_currentPlace);

		loadPlayer(m_currentPlace->getPlayerInfo(), spawnPoint,spawnName);
		setViewPointCenter(spawnPoint);

		//Make _blocked layer not visible
		TMXLayer* blocked = m_currentPlace->getTMXMap()->getLayer("_blocked");
		if (blocked != nullptr&& blocked != NULL)
			blocked->setVisible(false);
		TMXLayer* playerl = m_currentPlace->getTMXMap()->getLayer("_player");
		if (playerl != nullptr&& playerl != NULL)
			playerl->setVisible(false);

		//Enable shader program for rendertex
		GLProgram* p = static_cast<GLProgram*>(shaderPrograms->objectForKey(string(m_currentPlace->getLighting()->getCString())));
		if (p == nullptr){
			p = static_cast<GLProgram*>(shaderPrograms->objectForKey(string("default")));
			CCLOG("%s %s", "FATAL! Shader program not found:", m_currentPlace->getLighting()->getCString());
		}
		screenRenderTex->getSprite()->setShaderProgram(p);
		
	}
}

CCPoint isoPosFromObjDictionary(Dictionary* obj){
	int x = ((CCString*) obj->objectForKey("X"))->intValue();
	int y = ((CCString*) obj->objectForKey("Y"))->intValue();
	return ccp(x, y);
}

CCPoint TileLayer::spawnForName(String* spawnName, DSPlace* place){
	Dictionary* spawn = static_cast<Dictionary*>(place->getSpawns()->objectForKey(spawnName->getCString()));
	if (spawn == NULL)
	{
		CCLOG("%s %s", "FATAL! Spawn not found:", spawnName);
		return ccp(0,0);
	}
	return realCoordsForIsoPosition(isoPosFromObjDictionary(spawn));
}

void TileLayer::loadPlayer(String* playerInfo,CCPoint position,String* spawnPoint){
	string pstr = playerInfo->getCString();
	int spaceLoc = string(pstr).find_first_of(' ');
	String* name = new String(pstr.substr(0, spaceLoc));
	String* expr = new String(pstr.substr(spaceLoc+1));
	//Sprite 'name' is passed as DisplayName for player
	player = new Player(name,expr,name,spawnPoint);
	player->setPosition(position);
	m_currentPlace->getTMXMap()->addChild(player, m_currentPlace->getSpawnZ());
	npcs->push_back(player);
}

CCPoint TileLayer::isoCoordsForRealPosition(CCPoint point){
	float tw = m_currentPlace->getTMXMap()->getTileSize().width;
	float th = m_currentPlace->getTMXMap()->getTileSize().height;
	float mw = m_currentPlace->getTMXMap()->getMapSize().width;
	float mh = m_currentPlace->getTMXMap()->getMapSize().height;

	int posY = floorf(mh - point.x / tw + mw / 2 - point.y / th);
	int posX = floorf(mh + point.x / tw - mw / 2 - point.y / th);

	return (ccp(posX, posY));
}
CCPoint TileLayer::realCoordsForIsoPosition(CCPoint point){
	float halfMapWidth = m_currentPlace->getTMXMap()->getMapSize().width*0.5;
	float mapHeight = m_currentPlace->getTMXMap()->getMapSize().height;
	float tileWidth = m_currentPlace->getTMXMap()->getTileSize().width;
	float tileHeight = m_currentPlace->getTMXMap()->getTileSize().height;


	int x = halfMapWidth*tileWidth + tileWidth*point.x*0.5 - tileWidth*point.y*0.5;

	int y = (point.y + (mapHeight * tileWidth / 2) - (tileHeight / 2)) - ((point.y + point.x) *   tileHeight / 2) - tileHeight*0.25;

	return ccp(x, y);
}

void TileLayer::setViewPointCenter(cocos2d::CCPoint position){
	Size winSize = CCDirector::sharedDirector()->getWinSize();
	Size mapSize = m_currentPlace->getTMXMap()->getMapSize();
	Size tileSize = m_currentPlace->getTMXMap()->getTileSize();

	int x = MAX(position.x, winSize.width / 2);
	int y = MAX(position.y, winSize.height / 2);
	x = MIN(x, (mapSize.width * tileSize.width) - winSize.width / 2);
	y = MIN(y, (mapSize.height * tileSize.height) - winSize.height / 2);

	CCPoint actualPosition = ccp(x, y);
	CCPoint centerOfView = ccp(winSize.width / 2, winSize.height / 2);
	CCPoint viewPoint = ccpSub(centerOfView, actualPosition);
	this->setPosition(viewPoint);
}

bool TileLayer::isTileAtIsoCoordBlocked(CCPoint coord){
	TMXLayer* blocked = m_currentPlace->getTMXMap()->getLayer("_blocked");
	if (blocked != nullptr&& blocked != NULL)
	{
		if (coord.x<0 || coord.y <0 || coord.x>m_currentPlace->getTMXMap()->getMapSize().width ||
			coord.y>m_currentPlace->getTMXMap()->getMapSize().height)
			return true;
		Sprite * tile = blocked->getTileAt(coord);
		if (tile != nullptr&&tile != NULL)
			return true;
	}
	//check collidable objects
	CCTMXObjectGroup* objectGroup = m_currentPlace->getTMXMap()->getObjectGroup("_objects");
	for (int i = 0; i < objectGroup->getObjects()->count(); i++)
	{
		Dictionary* d = static_cast<CCDictionary*>(objectGroup->getObjects()->objectAtIndex(i));
		CCPoint p = isoPosFromObjDictionary(d);
		if (p.x == coord.x&&p.y == coord.y)
		{
			bool collidable = (static_cast<String*>(d->objectForKey("_collidable")))->intValue();
			if (collidable)
				return true;

			//Check for interact script and run it
			String* scriptLine = ((CCString*) d->objectForKey("_interactscript"));
			if (scriptLine != nullptr)
			{
				//Script to run
				ScriptManager::getInstance().addSingleATSCommandAsScript(string(scriptLine->getCString()));
			}
		}
		
	}
	return false;
}

bool TileLayer::canNPCMoveInDirection(NPC* npc,Direction dir){
	CCPoint playerIso = isoCoordsForRealPosition(npc->getPosition());
	CCPoint playerTargetIso = playerIso;
	Size tileSize = m_currentPlace->getTMXMap()->getTileSize();
	switch (dir){
	case NE:
		playerTargetIso.y -= 1;
		break;
	case NW:
		playerTargetIso.x -= 1;
		break;
	case SE:
		playerTargetIso.x += 1;
		break;
	case SW:
		playerTargetIso.y += 1;
		break;
	}
	//We have target real coord now, check for blocked or NPC at this position
	//Blocked tile
	if (isTileAtIsoCoordBlocked(playerTargetIso))
		return false;

	//NPCs 
	for (NPC* npc : *npcs){
		CCPoint npcPos = isoCoordsForRealPosition(npc->getPosition());
		if (npcPos.x == playerTargetIso.x&&npcPos.y == playerTargetIso.y)
			return false;
	}

	return true;
}

void TileLayer::deleteTileAtObjectNamed(String obj){
	TMXLayer* removableLayer = m_currentPlace->getTMXMap()->getLayer("_removable");
	if (removableLayer != nullptr){
		//Find object with name
		TMXObjectGroup* objects = m_currentPlace->getTMXMap()->getObjectGroup("_objects");
		if (objects != nullptr)
		{
			for (int i = 0; i < objects->getObjects()->count(); i++)
			{
				Dictionary* d = static_cast<CCDictionary*>(objects->getObjects()->objectAtIndex(i));
				if (strcmp(static_cast<CCString*>(d->objectForKey("name"))->getCString(),obj.getCString())==0)
				{
					//Found object
					CCPoint deletePos = isoPosFromObjDictionary(d);
					removableLayer->removeTileAt(deletePos);
					//Also remove blocked tile at this position (tentative feature)
					TMXLayer* blockedlayer = m_currentPlace->getTMXMap()->getLayer("_blocked");
					blockedlayer->removeTileAt(deletePos);
				}
			}
		}
	}
}

void TileLayer::setShader(String name){
	GLProgram* shader = static_cast<GLProgram*>(shaderPrograms->objectForKey(name.getCString()));
	if (shader != nullptr)
		screenRenderTex->getSprite()->setShaderProgram(shader);
}

void TileLayer::moveNPC(String name, Direction direction, int amount){
	NPC* npc = nullptr;
	for (NPC* n : *npcs){
		if (strcmp(name.getCString(), n->getNPCTag()->getCString()) == 0)
		{
			npc = n;
			break;
		}
	}
	if (npc != nullptr)
	{
		if (canNPCMoveInDirection(npc, direction))
		{
			Size mapSize = m_currentPlace->getTMXMap()->getMapSize();
			Size tileSize = m_currentPlace->getTMXMap()->getTileSize();
			switch (direction)
			{
			case NE:
				npc->moveNE(mapSize, tileSize);
				break;
			case NW:
				npc->moveNW(mapSize, tileSize);
				break;
			case SW:
				npc->moveSW(mapSize, tileSize);
				break;
			case SE:
				npc->moveSE(mapSize, tileSize);
				break;
			}
		}
		else
		{
			npc->turn(direction);
		}
	}

}

TileLayer::~TileLayer(){
	m_dsPlaces->removeAllObjects();
	screenRenderTex->release();
	CC_SAFE_RELEASE(m_currentPlace);
}

void TileLayer::playerInteractInFacingDirection(){
	CCPoint playerIso = isoCoordsForRealPosition(player->getPosition());
	CCPoint playerTargetIso = playerIso;
	Size tileSize = m_currentPlace->getTMXMap()->getTileSize();
	switch (player->getDirection()){
	case NE:
		playerTargetIso.y -= 1;
		break;
	case NW:
		playerTargetIso.x -= 1;
		break;
	case SE:
		playerTargetIso.x += 1;
		break;
	case SW:
		playerTargetIso.y += 1;
		break;
	}

	//Check for NPCs
	for (NPC* npc : *npcs){
		CCPoint npcPos = isoCoordsForRealPosition(npc->getPosition());
		if (npcPos.x == playerTargetIso.x&&npcPos.y == playerTargetIso.y)
		{
			DSScript* script = npc->getInteractScript();
			if (script != nullptr)
			{
				//Run the script
				ScriptManager::getInstance().addScript(script);
			}
		}
	}
}

void TileLayer::processInput(InputDirection dir){
	Size mapSize = m_currentPlace->getTMXMap()->getMapSize();
	Size tileSize = m_currentPlace->getTMXMap()->getTileSize();

	switch (dir){
	case UP:
		if (canNPCMoveInDirection(player, NE))
			player->moveNE(mapSize, tileSize);
		else
		{
			player->turn(NE);
			playerInteractInFacingDirection();
		}
		break;
	case DOWN:
		if (canNPCMoveInDirection(player, SW))
			player->moveSW(mapSize, tileSize);
		else
		{
			player->turn(SW);
			playerInteractInFacingDirection();
		}
		break;
	case LEFT:
		if (canNPCMoveInDirection(player, NW))
			player->moveNW(mapSize, tileSize);
		else
		{
			player->turn(NW);
			playerInteractInFacingDirection();
		}
		break;
	case RIGHT:
		if (canNPCMoveInDirection(player, SE))
			player->moveSE(mapSize, tileSize);
		else
		{
			player->turn(SE);
			playerInteractInFacingDirection();
		}
		break;
	}
	this->setViewPointCenter(player->getPosition());
}

void TileLayer::onTouchesEnded(std::vector<CCTouch*> pTouches, cocos2d::CCEvent *pEvent) {
	//Check ScriptManager is not swallowing input (Scripts can await input)
	if (ScriptManager::getInstance().isScriptAwaitingInput())
		return;

	for (int iTouchCount = 0; iTouchCount < pTouches.size(); iTouchCount++)
	{
		CCTouch* touch = pTouches[iTouchCount];

		Point touchLocation = touch->getLocationInView();
		touchLocation = CCDirector::sharedDirector()->convertToGL(touchLocation);
		touchLocation = this->convertToNodeSpace(touchLocation);

		if (m_currentPlace->getTMXMap() != nullptr)
		{

			

			Point playerPos = player->getPosition();
			Point diff = ccpSub(touchLocation, playerPos);

			//not being able to walk into something will attempt to interact with it
			if (abs(diff.x) > abs(diff.y)) {
				if (diff.x > 0) {
					processInput(RIGHT);
				}
				else {
					processInput(LEFT);
				}
			}
			else {
				if (diff.y > 0) {
					processInput(UP);
				}
				else {
					processInput(DOWN);
				}
			}

		}
	}
}

void TileLayer::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{

}
void TileLayer::keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	//Check ScriptManager is not swallowing input (Scripts can await input)
	if (ScriptManager::getInstance().isScriptAwaitingInput())
		return;

	switch (keyCode)
	{
	case EventKeyboard::KeyCode::KEY_ESCAPE:
		//XXX DEBUG
		CCDirector::getInstance()->end();
		break;
	case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
		processInput(RIGHT);
		break;
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
		processInput(LEFT);
		break;
	case EventKeyboard::KeyCode::KEY_UP_ARROW:
		processInput(UP);
		break;
	case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
		processInput(DOWN);
		break;
	}

}

void TileLayer::playBGM(String name, float volume){
	stringstream fstr;
	fstr << "Audio/" << name.getCString();
	CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(volume);
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(
		fstr.str().c_str(), true);
}

void TileLayer::playSFX(String name, float volume){
	stringstream fstr;
	fstr << "Audio/" << name.getCString();
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect(fstr.str().c_str());
	CocosDenshion::SimpleAudioEngine::sharedEngine()->setEffectsVolume(volume);
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(
		fstr.str().c_str());
}

void TileLayer::stopBGM(){
	CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
}

void TileLayer::loadShaderPrograms(){
	shaderPrograms = new Dictionary();
	std::vector<std::string> nameList = FileReader::ReadFile("Shaders/shaderlist", 'r');
	for (std::string s : nameList){
		if (s.length() > 0)
		{ 
			s.erase(s.find_last_not_of(" \n\r\t") + 1);
			GLProgram* prog = new GLProgram();
			prog->retain();
			stringstream fstr;
			fstr << "Shaders/" << s.c_str() << ".frag";
			prog->initWithVertexShaderFilename("Shaders/default.vert", fstr.str().c_str());
			//Cocos2dx attribs
			prog->addAttribute(kCCAttributeNamePosition, kCCVertexAttrib_Position);
			prog->addAttribute(kCCAttributeNameTexCoord, kCCVertexAttrib_TexCoords);
			prog->link();
			prog->updateUniforms();

			shaderPrograms->setObject(prog, string(s.c_str()));
		}
	}
}