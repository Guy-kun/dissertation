#include "AAChangeMap.h"

void AAChangeMap::runATS(TileLayer* tileLayer, UILayer* uiLayer, VNLayer* vnLayer){
	if (!_isDone)
	{
		tileLayer->makeMapCurrent(&targetMap, &targetSpawn);
		_isDone = true;
	}
}

bool AAChangeMap::isDone(){
	return _isDone;
}