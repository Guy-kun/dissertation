#ifndef __DS_AANPCCOMMAND__
#define __DS_AANPCCOMMAND__

#include "cocos2d.h"
#include "ATSNode.h"
#include "TileLayer.h"
#include "UILayer.h"
#include "VNLayer.h"

USING_NS_CC;

class DSScript;

class AANPCCommand : public ATSNode
{
public:
	AANPCCommand(DSScript* script) : ATSNode(script){ _isMoveNPC = false; };
	~AANPCCommand(){};

	void setTargetNPC(String target){ 
		// _this is a marker for an NPC script to affect itself. Pull the parent tag from the script to specify this NPC
		if (strcmp(target.getCString(), "_this") == 0)
			targetNPC = String(parentScript->getParentTag());
		else
			targetNPC = target; 
	};
	void setTargetDirection(String target){ 
		if (strcmp(target.getCString(), "NE") == 0)
			targetDirection = NE;
		else if (strcmp(target.getCString(), "NW") == 0)
			targetDirection = NW;
		else if (strcmp(target.getCString(), "SE") == 0)
			targetDirection = SE;
		else if (strcmp(target.getCString(), "SW") == 0)
			targetDirection = SW;
	};
	void setTargetAmount(String target){ targetAmount = atoi(target.getCString()); };

	void setIsMoveNPC(){ _isMoveNPC = true; };

	String getTargetNPC(){ return targetNPC; };
	Direction getTargetDirection(){ return targetDirection; };
	int getTargetAmount(){ return targetAmount; };

	void runATS(TileLayer* tileLayer, UILayer* uiLayer, VNLayer* vnLayer) override{
		if (!_isDone)
		{
			if (_isMoveNPC)
				tileLayer->moveNPC(targetNPC, targetDirection, targetAmount);
			_isDone = true;
		}
	};
	bool isDone(){
		return _isDone;
	};
private:
	String targetNPC;
	Direction targetDirection;
	int targetAmount;

	bool _isMoveNPC;

};

#endif