#include "AAShowHide.h"

void AAShowHide::runATS(TileLayer* tileLayer, UILayer* uiLayer, VNLayer* vnLayer){
	if (!_isDone)
	{
		if (_isShow)
			vnLayer->showImage(name, expression, with, at);
		else
			vnLayer->hideImage(name, with);
		_isDone = true;
	}
}

bool AAShowHide::isDone(){
	return _isDone;
}