#ifndef __DS_AASHOW__
#define __DS_AASHOW__

#include "cocos2d.h"
#include "ATSNode.h"
#include "TileLayer.h"
#include "UILayer.h"
#include "VNLayer.h"

USING_NS_CC;

class DSScript;

class AAShowHide : public ATSNode
{
public:
	AAShowHide(DSScript* script) : ATSNode(script){};
	~AAShowHide(){};

	String getName(){ return name; };
	String getExpression(){ return expression; };
	String getWith(){ return with; };
	String getAt(){ return at; };

	void setName(String w){ name = w; };
	void setExpression(String w){ expression = w; };
	void setWith(String w){ with = w; };
	void setAt(String w){ at = w; };
	void setisHide(){ _isShow = false; }

	void runATS(TileLayer* tileLayer, UILayer* uiLayer, VNLayer* vnLayer) override;
	bool isDone();
private:
	String name;
	String expression;
	String with;
	String at;

	bool _isShow = true;
};

#endif